import os
import random
import string
import unittest
from os import path

from click.testing import CliRunner

from img_renamer import img_renamer


class TestImgRenamer(unittest.TestCase):

    def test_mode_0_with_nothing_to_rename(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            result = runner.invoke(img_renamer.main, ['./data/', '-y'])
            self.assertEqual(result.output, 'Nothing to rename\n')
            self.assertTrue(path.exists('data/0000001.png'))
            self.assertTrue(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))
            self.assertFalse(path.exists('data/0000005.png'))

    def test_mode_0_with_all_to_rename(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            # Setup images
            os.mkdir('data')
            numbers = 123456789
            symbols = '{letters}' \
                      '{numbers}'.format(letters=string.ascii_lowercase,
                                         numbers=numbers)
            for i in range(0, 4):
                j = ''.join(random.choice(symbols) for _ in range(15))
                with open('data/{}.png'.format(j), 'w') as _:
                    pass
            # Actual test
            _ = runner.invoke(img_renamer.main, ['./data/', '-y'])
            self.assertTrue(path.exists('data/0000001.png'))
            self.assertTrue(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))
            self.assertFalse(path.exists('data/0000005.png'))

    def test_mode_0_with_custom_start(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            _ = runner.invoke(img_renamer.main, ['./data/', '-y', '-n', '3'])
            self.assertFalse(path.exists('data/0000001.png'))
            self.assertFalse(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))
            self.assertTrue(path.exists('data/0000005.png'))
            self.assertTrue(path.exists('data/0000006.png'))
            self.assertFalse(path.exists('data/0000007.png'))

    def test_mode_0_with_custom_zeroes(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            _ = runner.invoke(img_renamer.main, ['./data/', '-y', '-z', '3'])
            # default zeroes
            self.assertFalse(path.exists('data/0000001.png'))
            self.assertFalse(path.exists('data/0000002.png'))
            self.assertFalse(path.exists('data/0000003.png'))
            self.assertFalse(path.exists('data/0000004.png'))
            self.assertFalse(path.exists('data/0000005.png'))
            # 3 zeroes
            self.assertTrue(path.exists('data/001.png'))
            self.assertTrue(path.exists('data/002.png'))
            self.assertTrue(path.exists('data/003.png'))
            self.assertTrue(path.exists('data/004.png'))
            self.assertFalse(path.exists('data/005.png'))

    def test_mode_0_with_1_missing(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            os.rename('data/0000001.png', 'data/0000005.png')
            _ = runner.invoke(img_renamer.main, ['./data/', '-y', '-m', '0'])
            self.assertTrue(path.exists('data/0000001.png'))
            self.assertTrue(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))
            self.assertFalse(path.exists('data/0000005.png'))

    def test_mode_0_with_2_missing(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            os.rename('data/0000002.png', 'data/0000005.png')
            os.rename('data/0000003.png', 'data/0000006.png')
            _ = runner.invoke(img_renamer.main, ['./data/', '-y', '-m', '0'])
            self.assertTrue(path.exists('data/0000001.png'))
            self.assertTrue(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))
            self.assertFalse(path.exists('data/0000005.png'))

    def test_mode_0_with_new(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            with open('data/new.png', 'w') as _:
                pass
            _ = runner.invoke(img_renamer.main, ['./data/', '-y', '-m', '0'])
            self.assertTrue(path.exists('data/0000001.png'))
            self.assertTrue(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))
            self.assertTrue(path.exists('data/0000005.png'))

    def test_mode_0_with_missing_and_new(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            with open('data/new.png', 'w') as _:
                pass
            os.rename('data/0000002.png', 'data/0000005.png')
            _ = runner.invoke(img_renamer.main, ['./data/', '-y', '-m', '0'])
            self.assertTrue(path.exists('data/0000001.png'))
            self.assertTrue(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))

    def test_mode_0_with_logfile(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            with open('data/new.png', 'w') as _:
                pass
            self.assertFalse(path.exists('rename.log'))
            _ = runner.invoke(img_renamer.main, ['./data/', '-y', '-l'])
            # Read file line by line
            with open('rename.log', 'r') as fp:
                content = fp.readlines()
            self.assertTrue(content[0], 'new.png -> 0000005.png')
            self.assertTrue(path.exists('data/0000001.png'))
            self.assertTrue(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))
            self.assertTrue(path.exists('data/0000005.png'))
            self.assertFalse(path.exists('data/0000006.png'))
            self.assertTrue(path.exists('rename.log'))

    def test_mode_1_with_nothing_to_rename(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            result = runner.invoke(img_renamer.main, ['./data/', '-y', '-m',
                                                      '1'])
            self.assertEqual(result.output, 'Nothing to rename\n')
            self.assertTrue(path.exists('data/0000001.png'))
            self.assertTrue(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))
            self.assertFalse(path.exists('data/0000005.png'))

    def test_mode_1_with_custom_start(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            result = runner.invoke(img_renamer.main, ['./data/', '-y', '-n',
                                                      '10', '-m', '1'])
            self.assertEqual(result.output, 'Error: you can\'t change nro when'
                                            ' fixing numbering\n')

    def test_mode_1_with_custom_zeroes(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            result = runner.invoke(img_renamer.main, ['./data/', '-y', '-z',
                                                      '10', '-m', '1'])
            self.assertEqual(result.output, 'Error: you can\'t change zeroes'
                                            ' when fixing numbering\n')

    def test_mode_1_with_1_missing(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            os.rename('data/0000001.png', 'data/0000005.png')
            _ = runner.invoke(img_renamer.main, ['./data/', '-y', '-m', '1'])
            self.assertTrue(path.exists('data/0000001.png'))
            self.assertTrue(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))
            self.assertFalse(path.exists('data/0000005.png'))

    def test_mode_1_with_2_missing(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            os.rename('data/0000002.png', 'data/0000005.png')
            os.rename('data/0000003.png', 'data/0000006.png')
            _ = runner.invoke(img_renamer.main, ['./data/', '-y', '-m', '1'])
            self.assertTrue(path.exists('data/0000001.png'))
            self.assertTrue(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))
            self.assertFalse(path.exists('data/0000005.png'))
            self.assertFalse(path.exists('data/0000006.png'))

    def test_mode_1_with_new(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            with open('data/new.png', 'w') as _:
                pass
            _ = runner.invoke(img_renamer.main, ['./data/', '-y', '-m', '1'])
            self.assertTrue(path.exists('data/0000001.png'))
            self.assertTrue(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))
            self.assertTrue(path.exists('data/0000005.png'))

    def test_mode_1_with_missing_and_new(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            with open('data/new.png', 'w') as _:
                pass
            os.rename('data/0000002.png', 'data/0000005.png')
            _ = runner.invoke(img_renamer.main, ['./data/', '-y', '-m', '1'])
            self.assertTrue(path.exists('data/0000001.png'))
            self.assertTrue(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))
            self.assertTrue(path.exists('data/0000005.png'))

    def test_mode_1_with_logfile(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            create_files()
            with open('data/new.png', 'w') as _:
                pass
            self.assertFalse(path.exists('rename.log'))
            _ = runner.invoke(img_renamer.main, ['./data/', '-y', '-l', '-m',
                                                 '1'])
            # Read file line by line
            with open('rename.log', 'r') as fp:
                content = fp.readlines()
            self.assertTrue(content[0], 'new.png -> 0000005.png')
            self.assertTrue(path.exists('data/0000001.png'))
            self.assertTrue(path.exists('data/0000002.png'))
            self.assertTrue(path.exists('data/0000003.png'))
            self.assertTrue(path.exists('data/0000004.png'))
            self.assertTrue(path.exists('data/0000005.png'))
            self.assertFalse(path.exists('data/0000006.png'))
            self.assertTrue(path.exists('rename.log'))


def create_files():
    os.mkdir('data')
    with open('data/0000001.png', 'w') as _:
        pass
    with open('data/0000002.png', 'w') as _:
        pass
    with open('data/0000003.png', 'w') as _:
        pass
    with open('data/0000004.png', 'w') as _:
        pass
    with open('data/song.mp3', 'w') as _:
        pass
    with open('data/textfile.txt', 'w') as _:
        pass


if __name__ == '__main__':
    unittest.main()
